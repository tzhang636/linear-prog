function [  ] = linearDiscriminantTester( )
%This function makes sure you get the correct answer for a simple dataset
%with your findLinearDiscriminant implementation. 
data = [1 1 -1; 0 1 1; 0 0 1; 1 0 -1];

 [w,theta,delta] = findLinearDiscriminant(data);
 w
 theta
 delta
 weightCorrect = 1;
 if(abs(w(1) + 214.7 ) > .1 )
     weightCorrect = 0;
     
 end
 
 if(abs(w(2) -7.6 ) > .1 )
     weightCorrect = 0;
     
 end

 assert(abs(delta) <= .00001,'ERROR : The delta isnt correct');
 assert(weightCorrect==1 , 'ERROR : The weight vector isnt correct');
 assert( abs(theta -98.4) <= .1, 'ERROR : The theta isnt correct');
 
 data = [1 1 1; 1 1 -1; 0 0 -1; 1 0 -1];

 [w,theta,delta] = findLinearDiscriminant(data);
 weightCorrect = 1;
 if(abs(w(1) + 0.804 ) > .1 )
     weightCorrect = 0;
     
 end
 
 if(abs(w(2) -97.29 ) > .1 )
     weightCorrect = 0;
     
 end

 assert(abs(delta) >= .9,'ERROR : The delta isnt correct');
 assert(weightCorrect==1 , 'ERROR : The weight vector isnt correct');
 assert( abs(theta +96.49) <= .1, 'ERROR : The theta isnt correct');
 
 disp('Hooray, The code passed the tests ');
end

