% This script is to help you solve the conjunction learning problem.
% You do not need to change this file.
% Run this script after
% 1- finishing the implementation of findLinearDiscriminant function


%% experiment with given nd case
data = readFeatures('hw1conjunctions.txt',10);
[w,theta,delta] = findLinearDiscriminant(data);
w
theta
delta