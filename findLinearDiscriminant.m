% This function finds a linear discriminant using LP
% The linear discriminant is represented by 
% the weight vector w and the threshold theta.
% YOU NEED TO FINISH IMPLEMENTATION OF THIS FUNCTION.

%The appendix to the homework (final page of the PDF) 
% will walk you through an example for a different LP.

% The given linearDiscriminantTester.m function will test this code on 
% a few simple datasets [Although it does not mean your code will be bug free
% necessarily]. 

function [w,theta,delta] = findLinearDiscriminant(data)
%% setup linear program
[m, np1] = size(data); %size returns the dimensions of a matrix
n = np1-1; %n= the number of features (last column is "class")
%m = the number of training examples

%You need to take the data matrix and produce 3 arrays: A,b,c. 
%Let  i be the number of constraints of a LP (necessary inequalities) and j be the number of
%   variables of the LP  
% A  needs to have i rows and j columns .
%   (each row corresponding to an inequality constraint and each column corresponding to a variable)

%b needs to be i rows and 1 column  
%   each row  corresponding to an inequality in A (the same ordering).


% For the code below to work think of the inequalities as A*x <= b 
 %   where x is the variable input from the data. 
 

% c needs to have j rows and 1 column. Each row corresponds to a variable
%    (same order as in A) . c is the matrix that defines the linear function you are
%    minimize,  c(k) = the coeffecient of variable x_k  in the equation. 



% HINT: For the given code below to work define the ordering of variables
%    as  (w theta delta)  [where w and theta define the linear decision   boundary], how many variables total is that?:


%WRITE YOUR CODE HERE:
A = zeros(m+1,n+2);

for i=1:m
    for j=1:n
        A(i,j) = data(i,n+1) * data(i,j);
    end
end

for i=1:m
    A(i,n+1) = data(i,n+1);
end

for i=1:m+1
    A(i,n+2) = 1;
end

for j=1:n+1
    A(m+1,j) = 0;
end

b = ones([m+1,1]);
b(m+1,1) = 0;

c = zeros([n+2,1]);
c(n+2,1) = 1;

%% solve the linear program
%adjust for matlab input: A*x <= b
[t, z] = linprog(c, -A, -b);

%% obtain w,theta,delta from t vector
w = t(1:n);
theta = t(n+1);
delta = t(n+2);

end
